﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="HomeController.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The home controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Controllers
{
    using System.Web.Mvc;

    /// <summary>
    /// Main controller.
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Main page of application
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public ActionResult Index()
        {
            return this.View();
        }
    }
}