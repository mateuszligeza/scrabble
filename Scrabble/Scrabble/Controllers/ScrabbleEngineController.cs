﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScrabbleEngineController.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The scrabble engine controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Controllers
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    using Scrabble.Interfaces;
    using Scrabble.Models;

    /// <summary>
    /// The scrabble engine controller.
    /// </summary>
    public class ScrabbleEngineController : Controller
    {
        /// <summary>
        /// The _scrabble service.
        /// </summary>
        private readonly IScrabbleService scrabbleService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ScrabbleEngineController"/> class.
        /// </summary>
        /// <param name="scrabbleService">
        /// The scrabble service.
        /// </param>
        public ScrabbleEngineController(IScrabbleService scrabbleService)
        {
            this.scrabbleService = scrabbleService;
        }

        /// <summary>
        /// Gets the scrabble service.
        /// </summary>
        private IScrabbleService ScrabbleService
        {
            get
            {
                return this.scrabbleService;
            }
        }

        /// <summary>
        /// The find words.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public ActionResult FindWords(string letters)
        {
            letters = this.PreProcessLetters(letters);
            var findWordsResponse = new FindWordsResponse();
            var validationMessages = new List<string>();
            if (!this.ScrabbleService.ValidateLetters(letters, ref validationMessages))
            {
                findWordsResponse.ValidationMessages = validationMessages;
                findWordsResponse.Status = "ValidationError";
            }
            else
            {
                findWordsResponse.Words = this.ScrabbleService.FindWords(letters);
            }

            return this.Json(findWordsResponse, JsonRequestBehavior.DenyGet);
        }

        /// <summary>
        /// Preprocesses letters to simplify validation process.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string PreProcessLetters(string letters)
        {
            letters = letters ?? string.Empty;
            return letters.ToLowerInvariant();
        }
    }
}