﻿var scrabbleModule = angular.module('scrabbleModule', ['ui.bootstrap']);

scrabbleModule.controller('scrabbleController', ['$scope', '$http', function ($scope, $http) {
    $scope.alerts = [];
    $scope.words = [];
    $scope.letters = "";

    $scope.pageSize = 5;
    $scope.currentPage = 1;
    $scope.pagerSize = 5;

    var url = "/ScrabbleEngine/FindWords";
    
    var onAlert = function (msg) {
        $scope.addAlert({ type: 'danger', msg: msg });
    };

    $scope.addAlert = function (alert) {
        $scope.alerts.push(alert);
    };

    $scope.closeAlert = function (index) {
        $scope.alerts.splice(index, 1);
    };

    $scope.clearAlerts = function () {
        while ($scope.alerts.length) {
            $scope.alerts.pop();
        }
    };

    var onValidationError = function (data) {
        if (typeof data != "undefined" && data != null && data.hasOwnProperty('ValidationMessages') && data.ValidationMessages != null && data.ValidationMessages.length > 0) {
            $('html, body').animate({ scrollTop: 0 }, 'fast');
            for (var i = 0; i < data.ValidationMessages.length; i++) {
                onAlert(data.ValidationMessages[i]);
            }
        }
    };

    var clearWords = function() {
        while ($scope.words.length) {
            $scope.words.pop();
        }
    };

    var addWords = function (words) {
        Array.prototype.push.apply($scope.words, words);
    };

    $scope.getLetters = function (word) {
        return word.split("");
    };

    $scope.submit = function () {
        $scope.clearAlerts();
        clearWords();
        var postData = { letters: $scope.letters };
        $http.post(url, postData).success(function (data) {
            if (data.Status === "ValidationError") {
                onValidationError(data);
            } else {
                addWords(data.Words);
            }
        });
    };
}]);

scrabbleModule.filter('startFrom', function () {
    return function (input, start) {
        start = +start; //parse to int
        return (input || []).slice(start);
    }
});