﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DescendingScrabbleWordScoreComparer.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The descending scrabble word score comparer.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Scrabble.Common
{
    using System.Collections.Generic;

    using Scrabble.Models;

    /// <summary>
    /// The descending scrabble word score comparer.
    /// </summary>
    public class DescendingScrabbleWordScoreComparer : IComparer<ScrabbleWord>
    {
        /// <summary>
        /// The descending compare function for ScrabbleWord objects.
        /// </summary>
        /// <param name="x">
        /// First word.
        /// </param>
        /// <param name="y">
        /// Second word.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int Compare(ScrabbleWord x, ScrabbleWord y)
        {
            return y.Score - x.Score;
        }
    }
}