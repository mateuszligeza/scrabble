﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScrabbleExtensions.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The scrabble extensions.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Common
{
    using System.Collections.Generic;

    using Scrabble.Interfaces;

    /// <summary>
    /// The scrabble extensions.
    /// </summary>
    public static class ScrabbleExtensions
    {
        /// <summary>
        /// The calculate word score.
        /// </summary>
        /// <param name="word">
        /// The word.
        /// </param>
        /// <param name="lettersProvider">
        /// The letters provider.
        /// </param>
        /// <returns>
        /// The <see cref="int"/> word score.
        /// </returns>
        public static int CalculateWordScore(this string word, IScrabbleLettersProvider lettersProvider)
        {
            var score = 0;
            foreach (var letter in word.ToCharArray())
            {
                score += lettersProvider.Letters[letter].Points;
            }

            return score;
        }

        /// <summary>
        /// Calculates word letters.
        /// </summary>
        /// <param name="word">
        /// The word.
        /// </param>
        /// <returns>
        /// The <see cref="Dictionary"/> containing letters count.
        /// </returns>
        public static Dictionary<char, int> CalculateWordLetters(this string word)
        {
            var wordLetters = new Dictionary<char, int>();
            foreach (var letter in word.ToCharArray())
            {
                if (!wordLetters.ContainsKey(letter))
                {
                    wordLetters.Add(letter, 0);
                }

                wordLetters[letter] += 1;
            }

            return wordLetters;
        }
    }
}