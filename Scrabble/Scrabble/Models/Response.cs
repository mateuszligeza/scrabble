﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Response.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The response.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// The response.
    /// </summary>
    public class Response
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Response"/> class.
        /// </summary>
        public Response()
        {
            this.ValidationMessages = new List<string>();
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the validation messages.
        /// </summary>
        public List<string> ValidationMessages { get; set; }
    }
}