﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FindWordsResponse.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The find words response.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// The find words response.
    /// </summary>
    public class FindWordsResponse : Response
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FindWordsResponse"/> class.
        /// </summary>
        public FindWordsResponse()
        {
            this.Words = new List<ScrabbleWord>();
        }

        /// <summary>
        /// Gets or sets the words.
        /// </summary>
        public List<ScrabbleWord> Words { get; set; } 
    }
}