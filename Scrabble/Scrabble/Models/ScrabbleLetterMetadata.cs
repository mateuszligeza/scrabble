﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScrabbleLetterMetadata.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The scrabble letter metadata.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Models
{
    /// <summary>
    /// The scrabble letter metadata.
    /// </summary>
    public class ScrabbleLetterMetadata
    {
        /// <summary>
        /// Gets or sets the letter points.
        /// </summary>
        public int Points { get; set; }

        /// <summary>
        /// Gets or sets the number of letters.
        /// </summary>
        public int Count { get; set; }
    }
}