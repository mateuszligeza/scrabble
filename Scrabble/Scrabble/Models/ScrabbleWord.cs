﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScrabbleWord.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The scrabble word.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Models
{
    using System;

    /// <summary>
    /// The scrabble word.
    /// </summary>
    public class ScrabbleWord
    {
        /// <summary>
        /// Gets or sets the word.
        /// </summary>
        public string Word { get; set; }

        /// <summary>
        /// Gets or sets the score.
        /// </summary>
        public int Score { get; set; }
    }
}