﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnityConfig.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The unity config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble
{
    using System.Web.Mvc;
    using Microsoft.Practices.Unity;

    using Scrabble.Implementation;
    using Scrabble.Interfaces;

    using Unity.Mvc5;

    /// <summary>
    /// The unity config.
    /// </summary>
    public static class UnityConfig
    {
        /// <summary>
        /// The register injectable components.
        /// </summary>
        public static void RegisterComponents()
        {
            var container = new UnityContainer();
            container.RegisterType<IScrabbleLettersProvider, EnglishScrabbleLettersProvider>();
            container.RegisterType<IScrabbleWordProvider, TxtFileScrabbleWordProvider>();
            container.RegisterType<ILetterValidator, LetterValidator>();
            container.RegisterType<IScrabbleService, ScrabbleService>(new ContainerControlledLifetimeManager());
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}