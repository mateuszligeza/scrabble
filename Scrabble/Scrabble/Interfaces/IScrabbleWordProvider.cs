﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IScrabbleWordProvider.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The ScrabbleWordProvider interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// The ScrabbleWordProvider interface.
    /// </summary>
    public interface IScrabbleWordProvider
    {
        /// <summary>
        /// Gets the words.
        /// </summary>
        IEnumerable<string> Words { get; } 
    }
}
