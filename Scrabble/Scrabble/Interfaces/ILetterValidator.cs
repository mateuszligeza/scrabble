﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ILetterValidator.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The LetterValidator interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// The LetterValidator interface.
    /// </summary>
    public interface ILetterValidator
    {
        /// <summary>
        /// Validates supplied letters.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>  
        /// <param name="lettersProvider">
        /// The letters provider.
        /// </param> 
        /// <param name="validationMessages">
        /// Collection of validation errors.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> indicating if letters are valid.
        /// </returns>
        bool ValidateLetters(string letters, IScrabbleLettersProvider lettersProvider, ref List<string> validationMessages);
    }
}
