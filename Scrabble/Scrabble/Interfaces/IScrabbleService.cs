﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IScrabbleService.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The ScrabbleService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Interfaces
{
    using System.Collections.Generic;
    using Scrabble.Models;

    /// <summary>
    /// The ScrabbleService interface.
    /// </summary>
    public interface IScrabbleService
    {
        /// <summary>
        /// Validates supplied letters.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>
        /// <param name="validationMessages">
        /// Collection of validation errors.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> indicating if letters are valid.
        /// </returns>
        bool ValidateLetters(string letters, ref List<string> validationMessages);

        /// <summary>
        /// Finds words that can be made with supplied letters.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>
        /// <returns>
        /// The <see cref="List"/> with found words.
        /// </returns>
        List<ScrabbleWord> FindWords(string letters);
    }
}
