﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IScrabbleLettersProvider.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The ScrabbleLettersProvider interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Interfaces
{
    using System.Collections.Generic;

    using Scrabble.Models;

    /// <summary>
    /// The ScrabbleLettersProvider interface.
    /// </summary>
    public interface IScrabbleLettersProvider
    {
        /// <summary>
        /// Gets the letters.
        /// </summary>
        Dictionary<char, ScrabbleLetterMetadata> Letters { get; }

        /// <summary>
        /// Gets the wildcard character
        /// </summary>
        char WildcardCharacter { get; }
    }
}
