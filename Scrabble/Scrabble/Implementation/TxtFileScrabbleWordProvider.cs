﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TxtFileScrabbleWordProvider.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The txt file scrabble word provider.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Scrabble.Implementation
{
    using System.Collections.Generic;
    using System.IO;
    using System.Web;
    using Scrabble.Interfaces;

    /// <summary>
    /// The txt file scrabble word provider.
    /// </summary>
    public class TxtFileScrabbleWordProvider : IScrabbleWordProvider
    {
        /// <summary>
        /// Gets the words.
        /// </summary>
        public IEnumerable<string> Words
        {
            get
            {
                using (var file = new StreamReader(HttpContext.Current.Server.MapPath("~/Content/dictionary.txt")))
                {
                    string line;
                    while ((line = file.ReadLine()) != null)
                    {
                        yield return line;
                    }
                }
            }
        }
    }
}