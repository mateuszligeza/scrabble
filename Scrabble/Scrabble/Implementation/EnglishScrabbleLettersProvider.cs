﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="EnglishScrabbleLettersProvider.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The english scrabble letters provider.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Implementation
{
    using System.Collections.Generic;
    using Scrabble.Interfaces;
    using Scrabble.Models;

    /// <summary>
    /// The english scrabble letters provider.
    /// </summary>
    public class EnglishScrabbleLettersProvider : IScrabbleLettersProvider
    {
        private const char InternalWildcardCharacter = '?';

        /// <summary>
        /// The internal letters.
        /// </summary>
        private static readonly Dictionary<char, ScrabbleLetterMetadata> InternalLetters = new Dictionary<char, ScrabbleLetterMetadata>
                                                                                               {
                                                                                                   {InternalWildcardCharacter, new ScrabbleLetterMetadata {Points = 0, Count = 2}}, 
                                                                                                   {'e', new ScrabbleLetterMetadata {Points = 1, Count = 12}}, 
                                                                                                   {'a', new ScrabbleLetterMetadata {Points = 1, Count = 9}}, 
                                                                                                   {'i', new ScrabbleLetterMetadata {Points = 1, Count = 9}}, 
                                                                                                   {'o', new ScrabbleLetterMetadata {Points = 1, Count = 8}}, 
                                                                                                   {'n', new ScrabbleLetterMetadata {Points = 1, Count = 6}}, 
                                                                                                   {'r', new ScrabbleLetterMetadata {Points = 1, Count = 6}}, 
                                                                                                   {'t', new ScrabbleLetterMetadata {Points = 1, Count = 6}}, 
                                                                                                   {'l', new ScrabbleLetterMetadata {Points = 1, Count = 4}}, 
                                                                                                   {'s', new ScrabbleLetterMetadata {Points = 1, Count = 4}}, 
                                                                                                   {'u', new ScrabbleLetterMetadata {Points = 1, Count = 4}}, 
                                                                                                   {'d', new ScrabbleLetterMetadata {Points = 2, Count = 4}}, 
                                                                                                   {'g', new ScrabbleLetterMetadata {Points = 2, Count = 3}}, 
                                                                                                   {'b', new ScrabbleLetterMetadata {Points = 3, Count = 2}}, 
                                                                                                   {'c', new ScrabbleLetterMetadata {Points = 3, Count = 2}}, 
                                                                                                   {'m', new ScrabbleLetterMetadata {Points = 3, Count = 2}}, 
                                                                                                   {'p', new ScrabbleLetterMetadata {Points = 3, Count = 2}}, 
                                                                                                   {'f', new ScrabbleLetterMetadata {Points = 4, Count = 2}}, 
                                                                                                   {'h', new ScrabbleLetterMetadata {Points = 4, Count = 2}}, 
                                                                                                   {'v', new ScrabbleLetterMetadata {Points = 4, Count = 2}}, 
                                                                                                   {'w', new ScrabbleLetterMetadata {Points = 4, Count = 2}}, 
                                                                                                   {'y', new ScrabbleLetterMetadata {Points = 4, Count = 2}}, 
                                                                                                   {'k', new ScrabbleLetterMetadata {Points = 5, Count = 1}}, 
                                                                                                   {'j', new ScrabbleLetterMetadata {Points = 8, Count = 1}}, 
                                                                                                   {'x', new ScrabbleLetterMetadata {Points = 8, Count = 1}}, 
                                                                                                   {'q', new ScrabbleLetterMetadata {Points = 10, Count = 1}}, 
                                                                                                   {'z', new ScrabbleLetterMetadata {Points = 10, Count = 1}}
                                                                                               };

        /// <summary>
        /// Gets the letters.
        /// </summary>
        public Dictionary<char, ScrabbleLetterMetadata> Letters
        {
            get
            {
                return InternalLetters;
            }
        }


        /// <summary>
        /// Gets the wildcard character.
        /// </summary>
        public char WildcardCharacter
        {
            get
            {
                return InternalWildcardCharacter;
            }
        }
    }
}