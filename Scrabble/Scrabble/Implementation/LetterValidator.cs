﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="LetterValidator.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The letter validator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Implementation
{
    using System.Collections.Generic;

    using Scrabble.Common;
    using Scrabble.Interfaces;

    /// <summary>
    /// The letter validator.
    /// </summary>
    public class LetterValidator : ILetterValidator
    {
        /// <summary>
        /// Validates supplied letters.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>
        /// <param name="lettersProvider">
        /// The letters provider.
        /// </param> 
        /// <param name="validationMessages">
        /// Collection of validation errors.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> indicating if letters are valid.
        /// </returns>
        public bool ValidateLetters(string letters, IScrabbleLettersProvider lettersProvider, ref List<string> validationMessages)
        {
            var isValid = true;
            isValid &= ValidateMinLength(letters, ref validationMessages);
            isValid &= ValidateMaxLength(letters, ref validationMessages);
            isValid &= ValidateCharacters(letters, lettersProvider, ref validationMessages);
            return isValid;
        }

        /// <summary>
        /// Validate min length.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>
        /// <param name="validationMessages">
        /// The validation messages.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> validation result.
        /// </returns>
        private static bool ValidateMinLength(string letters, ref List<string> validationMessages)
        {
            var isValid = letters.Length > 1;
            if (!isValid)
            {
                validationMessages.Add("You should provide at least two letters");
            }

            return isValid;
        }

        /// <summary>
        /// Validate max length.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>
        /// <param name="validationMessages">
        /// The validation messages.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> validation result.
        /// </returns>
        private static bool ValidateMaxLength(string letters, ref List<string> validationMessages)
        {
            var isValid = letters.Length < 8;
            if (!isValid)
            {
                validationMessages.Add("You should provide up to seven letters");
            }

            return isValid;
        }

        /// <summary>
        /// Validate characters.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>
        /// <param name="lettersProvider">
        /// The letters provider.
        /// </param>
        /// <param name="validationMessages">
        /// The validation messages.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> validation result.
        /// </returns>
        private static bool ValidateCharacters(string letters, IScrabbleLettersProvider lettersProvider, ref List<string> validationMessages)
        {
            var isValid = true;
            var letterCount = letters.CalculateWordLetters();
            foreach (var letterInfo in letterCount)
            {
                var isValidCharacterType = ValidateCharacterType(letterInfo.Key, lettersProvider, ref validationMessages);
                if (isValidCharacterType)
                {
                    isValid &= ValidateCharacterCount(letterInfo.Key, letterInfo.Value, lettersProvider, ref validationMessages);
                }
                else
                {
                    isValid = false;
                }
            }

            return isValid;
        }

        /// <summary>
        /// Validate character type.
        /// </summary>
        /// <param name="letter">
        /// The letter.
        /// </param>
        /// <param name="lettersProvider">
        /// The letters provider.
        /// </param>
        /// <param name="validationMessages">
        /// The validation messages.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> validation result.
        /// </returns>
        private static bool ValidateCharacterType(char letter, IScrabbleLettersProvider lettersProvider, ref List<string> validationMessages)
        {
            var isValid = lettersProvider.Letters.ContainsKey(letter);
            if (!isValid)
            {
                validationMessages.Add("You provided invalid letter: " + letter);
            }

            return isValid;
        }

        /// <summary>
        /// Validate character count.
        /// </summary>
        /// <param name="letter">
        /// The letter.
        /// </param>
        /// <param name="count">
        /// The count.
        /// </param>
        /// <param name="lettersProvider">
        /// The letters provider.
        /// </param>
        /// <param name="validationMessages">
        /// The validation messages.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/> validation result.
        /// </returns>
        private static bool ValidateCharacterCount(char letter, int count, IScrabbleLettersProvider lettersProvider, ref List<string> validationMessages)
        {
            var isValid = count <= lettersProvider.Letters[letter].Count;
            if (!isValid)
            {
                validationMessages.Add("You provided too many letters: " + letter);
            }

            return isValid;
        }
    }
}