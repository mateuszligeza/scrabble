﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScrabbleService.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The scrabble service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Implementation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Scrabble.Common;
    using Scrabble.Interfaces;
    using Scrabble.Models;

    /// <summary>
    /// The scrabble service.
    /// </summary>
    public class ScrabbleService : IScrabbleService
    {
        /// <summary>
        /// The scrabble words scores.
        /// </summary>
        private readonly Dictionary<string, int> scrabbleWordsScores = new Dictionary<string, int>();

        /// <summary>
        /// The words with specified lengths.
        /// </summary>
        private readonly Dictionary<int, List<string>> wordsWithSpecifiedLengths = new Dictionary<int, List<string>>();

        /// <summary>
        /// The letters provider.
        /// </summary>
        private readonly IScrabbleLettersProvider lettersProvider;

        /// <summary>
        /// The letters validator.
        /// </summary>
        private readonly ILetterValidator letterValidator;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="ScrabbleService"/> class.
        /// </summary>
        /// <param name="wordProvider">
        /// The word provider.
        /// </param>
        /// <param name="lettersProvider">
        /// The letters provider.
        /// </param>
        /// <param name="letterValidator">
        /// The letters validator.
        /// </param>
        public ScrabbleService(IScrabbleWordProvider wordProvider, IScrabbleLettersProvider lettersProvider, ILetterValidator letterValidator)
        {
            this.lettersProvider = lettersProvider;
            this.letterValidator = letterValidator;
            this.CreateInternalWordDictionaries(wordProvider);
        }

        /// <summary>
        /// Gets the scrabble words scores.
        /// </summary>
        private Dictionary<string, int> ScrabbleWordsScores
        {
            get
            {
                return this.scrabbleWordsScores;
            }
        }

        /// <summary>
        /// Gets the words with specified lengths.
        /// </summary>
        private Dictionary<int, List<string>> WordsWithSpecifiedLengths
        {
            get
            {
                return this.wordsWithSpecifiedLengths;
            }
        }

        /// <summary>
        /// Gets the letters provider.
        /// </summary>
        private IScrabbleLettersProvider LettersProvider
        {
            get
            {
                return this.lettersProvider;
            }
        }

        /// <summary>
        /// Gets the letters validator.
        /// </summary>
        private ILetterValidator LetterValidator
        {
            get
            {
                return this.letterValidator;
            }
        }

        /// <summary>
        /// The validate letters.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>
        /// <param name="validationMessages">
        /// The validation messages.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ValidateLetters(string letters, ref List<string> validationMessages)
        {
            return this.LetterValidator.ValidateLetters(letters, this.LettersProvider, ref validationMessages);
        }

        /// <summary>
        /// The find words.
        /// </summary>
        /// <param name="letters">
        /// The letters.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ScrabbleWord> FindWords(string letters)
        {
            var result = new List<ScrabbleWord>();
            var inputLettersDict = letters.CalculateWordLetters();
            var allowedWildcards = this.CalculateAllowedWildcards(inputLettersDict);
            for (int i = 0; i <= letters.Length; i++)
            {
                if (this.WordsWithSpecifiedLengths.ContainsKey(i))
                {
                    var wordsWithTheSameOrLessLetters = this.WordsWithSpecifiedLengths[i];
                    foreach (var word in wordsWithTheSameOrLessLetters)
                    {
                        var wordLettersDict = word.CalculateWordLetters();
                        var wildcardsLeft = allowedWildcards;
                        foreach (var letterMetadata in wordLettersDict)
                        {
                            if (!inputLettersDict.ContainsKey(letterMetadata.Key))
                            {
                                wildcardsLeft -= letterMetadata.Value;
                            }
                            else
                            {
                                wildcardsLeft = wildcardsLeft
                                                   - Math.Max(letterMetadata.Value - inputLettersDict[letterMetadata.Key], 0);
                            }

                            if (wildcardsLeft < 0)
                            {
                                break;
                            }
                        }

                        if (wildcardsLeft >= 0)
                        {
                            result.Add(new ScrabbleWord
                                           {
                                               Word = word, 
                                               Score = this.ScrabbleWordsScores[word]
                                           });
                        }
                    }
                }
            }
            result.Sort(new DescendingScrabbleWordScoreComparer());
            return result;
        }

        /// <summary>
        /// The calculate allowed wildcards.
        /// </summary>
        /// <param name="inputLettersDict">
        /// The input letters dictionary.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int CalculateAllowedWildcards(Dictionary<char, int> inputLettersDict)
        {
            return inputLettersDict.ContainsKey(this.LettersProvider.WildcardCharacter)
                       ? inputLettersDict[this.LettersProvider.WildcardCharacter]
                       : 0;
        }

        /// <summary>
        /// The create internal word dictionaries.
        /// </summary>
        /// <param name="wordProvider">
        /// The word provider.
        /// </param>
        private void CreateInternalWordDictionaries(IScrabbleWordProvider wordProvider)
        {
            foreach (var word in wordProvider.Words)
            {
                this.StoreWordWithScore(word);
                this.StoreWordWithSpecifiedLength(word);
            }
        }

        /// <summary>
        /// The store word with score.
        /// </summary>
        /// <param name="word">
        /// The word.
        /// </param>
        private void StoreWordWithScore(string word)
        {
            var score = word.CalculateWordScore(this.LettersProvider);
            this.ScrabbleWordsScores.Add(word, score);
        }

        /// <summary>
        /// The store word with specified length.
        /// </summary>
        /// <param name="word">
        /// The word.
        /// </param>
        private void StoreWordWithSpecifiedLength(string word)
        {
            int length = word.Length;
            if (!this.WordsWithSpecifiedLengths.ContainsKey(length))
            {
                this.WordsWithSpecifiedLengths.Add(length, new List<string>());
            }

            this.WordsWithSpecifiedLengths[length].Add(word);
        }
    }
}