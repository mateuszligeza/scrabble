﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UnitTest.cs" company="Mateusz Ligęza">
//   Copyright (c) Mateusz Ligęza 2015. All rights reserved.
// </copyright>
// <summary>
//   The unit test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Scrabble.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>
    /// This should be unit tests class.
    /// </summary>
    [TestClass]
    public class UnitTest
    {
        /// <summary>
        /// I was to tired to write tests - sorry.
        /// </summary>
        [TestMethod]
        public void ThereAreNoTests()
        {
        }
    }
}
